from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers
from knox import views as knox_views

from .views import (RegisterApi, LoginApiView, UserDataApi,)

router = routers.DefaultRouter()

urlpatterns = [
    path('auth', include('knox.urls')),
    path('user/logout', knox_views.LogoutView.as_view(), name="logout_view"),
    path('auth/register/', RegisterApi.as_view()),
    path('user/login/', LoginApiView.as_view()),
    path('user/data/', UserDataApi.as_view()),
]
urlpatterns += router.urls